create_for_username="gtsatsis"

# Update the repository cache, and upgrade all packages on the host without intervention.
apt update
apt upgrade -yqq

# Install usually-required packages. (Usually, without intervention)
apt install \
  sudo \
  curl \
  wget \
  gnupg2 \
  git \
  python3 \
  python3-pip -yqq

# Stricten up the SSH daemon configuration.
mv /etc/ssh/sshd_config /etc/ssh/originalSSHdConfiguration
cat > /etc/ssh/sshd_config <<EOL
PermitRootLogin no
PubkeyAuthentication yes
PasswordAuthentication no
UsePAM yes
PrintMotd no
AcceptEnv LANG LC_*
Subsystem	sftp	/usr/lib/openssh/sftp-server
EOL

# Restart the SSH daemon
systemctl restart sshd

# Create user (with home directory), grant sudo, and prompt for a password.
useradd -m $create_for_username
usermod -aG sudo $create_for_username
chsh -s /bin/bash $create_for_username

passwd $create_for_username

# Create ~/.ssh directory and access the authorized_keys file.
mkdir "/home/$create_for_username/.ssh"
touch "/home/$create_for_username/.ssh/authorized_keys"

vi "/home/$create_for_username/.ssh/authorized_keys"
